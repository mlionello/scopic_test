#include "SceneManager.h"

const char *SceneManager::vertexShaderShapes =
    "#version 330\n"
    "in vec3 vertices;\n"
    "in vec3 normals;\n"

    "out vec3 fnormals;\n"
    "out vec3 fpos;\n"

    "uniform mat4 vp;\n"
    "uniform mat4 model;\n"

    "void main()\n"
    "{\n"
    "    fpos = vec3(model * vec4(vertices, 1.0f));\n"
    "    fnormals = mat3(transpose(inverse(model))) * normals;\n"
    "    gl_Position = vp * vec4(fpos, 1.0f);\n"

    "}\n";

const char *SceneManager::fragmentShaderShapes =

    "#version 330\n"
    "in vec3 fpos;\n"
    "in vec3 fnormals;\n"
    "out vec4 color;\n"

    "struct Material{\n"
        "vec3 ambient;\n"
        "vec3 diffuse;\n"
        "vec3 specular;\n"
        "float shininess;\n"
    "};\n"

    "uniform vec3 view_pos;\n"
    "uniform Material material;\n"

    "vec3 l_position = vec3(0.0f, 0.0f, 0.0f);\n"
    "vec3 l_ambient  = vec3(0.3f, 0.1, 0.1f);\n"
    "vec3 l_diffuse = vec3(0.8f, 0.8, 0.7f);\n"
    "vec3 l_specular = vec3(1.0f, 1.0f, 1.0f);\n"

    "void main(){\n"
    "vec3 ambient = l_ambient * material.ambient;\n"
  	
    // diffuse 
    "vec3 norm = normalize(fnormals);\n"
    "vec3 light_dir = normalize(l_position - fpos);\n"
    "float diff = max(dot(norm, light_dir), 0.0);\n"
    "vec3 diffuse = l_diffuse * (diff * material.diffuse);\n"
    
    // specular
    "vec3 view_dir = normalize(view_pos- fpos);\n"
    "vec3 ref_dir = reflect(-light_dir, norm);  \n"
    "float spec = pow(max(dot(view_dir, ref_dir), 0.0), material.shininess);\n"
    "vec3 specular = l_specular * (spec * material.specular);  \n"
        
    "vec3 result = ambient + diffuse + specular;\n"

    "    color =  vec4(result, 1.0);\n"
    "}\n";

const char *SceneManager::vertexShaderAxis =
    "#version 330\n"
    "in vec3 vertices;\n"
    "out vec3 fpos;\n"

    "uniform mat4 vp;\n"

    "void main()\n"
    "{\n"
    "    fpos = vertices;\n"
    "    gl_Position = vp * vec4(fpos, 1.0f);\n"

    "}\n";

const char *SceneManager::fragmentShaderAxis =
    "#version 330\n"
    "in vec3 fpos;\n"
    "out vec4 color;\n"

    "void main(){\n"
    "    color =  vec4(fpos, 1.0);\n"
    "}\n";

SceneManager::SceneManager()
{
}

SceneManager::~SceneManager()
{
    if (m_program == nullptr)
        return;
    delete m_program;
    m_program = 0;

    delete m_axis_program;
    m_axis_program = 0;

    for(auto it = this->m_shapes.begin(); it != this->m_shapes.end(); ++it){
        delete *it;
    }
}

void SceneManager::initGL()
{
    m_camera.setToIdentity();
    m_camera.setTranslation(0, 0, -15);

    glClearColor(0.1, 0.1, 0.1, 1);

    //Shape program
    m_program = new QOpenGLShaderProgram();
    m_program->addShaderFromSourceCode(QOpenGLShader::Vertex, SceneManager::vertexShaderShapes);
    m_program->addShaderFromSourceCode(QOpenGLShader::Fragment, SceneManager::fragmentShaderShapes);
    m_program->link();

    m_program->bind();
    m_program->bindAttributeLocation("vertices", 0);
    m_program->bindAttributeLocation("normals", 1);
    m_program->link();
    m_vp = m_program->uniformLocation("vp");
    m_model = m_program->uniformLocation("model");

    AssetLoader *al = AssetLoader::getInstance();
    Mesh *mesh = al->getMesh("Cube");
    Material *mat1 = al->getMaterial("Material1");
    Material *mat2 = al->getMaterial("Material2");
    Material *mat3 = al->getMaterial("Material3");

    mesh->init(m_program);
    mat1->init(m_program);
    mat2->init(m_program);
    mat3->init(m_program);

    m_program->release();

    //Axis program
    glLineWidth(2);
    m_axis_program = new QOpenGLShaderProgram();
    m_axis_program->addShaderFromSourceCode(QOpenGLShader::Vertex, SceneManager::vertexShaderAxis);
    m_axis_program->addShaderFromSourceCode(QOpenGLShader::Fragment, SceneManager::fragmentShaderAxis);
    m_axis_program->link();

    m_axis_program->bind();
    m_axis_program->bindAttributeLocation("vertices", 0);
    m_axis_program->link();
    m_axis_vp = m_axis_program->uniformLocation("vp");
    axis.init(m_axis_program);
    m_axis_program->release();

}

void SceneManager::resizeScreen(int h, int w)
{
    qreal aspect = qreal(w) / qreal(h ? h : 1);
    const qreal zNear = 3.0, zFar = 100.0, fov = 45.0;
    m_proj.setToIdentity();
    m_proj.perspective(fov, aspect, zNear, zFar);
}

void SceneManager::drawAll()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    //glEnable(GL_CULL_FACE);

    //Axis
    m_axis_program->bind();
    m_axis_program->setUniformValue(m_axis_vp, m_proj *  m_camera.toMatrix());
    QOpenGLVertexArrayObject::Binder vaoBinder(&axis.getVAO());
    glDrawArrays(GL_LINES, 0, 9);
    m_axis_program->release();

    //Shapes
    m_program->bind();
    m_program->setUniformValue(m_vp, m_proj *  m_camera.toMatrix());

    GLuint cam_p = m_program->uniformLocation("view_pos");
    QVector3D camPos = -m_camera.translation();
    m_program->setUniformValue(cam_p, camPos.x(), camPos.y(), camPos.z());

    size_t i;
    for(i = 0; i < m_shapes.size(); ++i){
        m_shapes.at(i)->getMaterial()->use(m_program);
        QOpenGLVertexArrayObject::Binder vaoBinder(&m_shapes.at(i)->getMesh()->getVAO());
        m_program->setUniformValue(m_model, m_shapes.at(i)->getTransform());

        glDrawArrays(GL_TRIANGLES, 0, 36);
    }

    m_program->release();
}

void SceneManager::handleKeyPressed(QKeyEvent *event)
{
    static QVector3D t;
    if (event->isAutoRepeat())
    {
        event->ignore();
    }
    else
    {
        static const float transSpeed = 0.5f;

        // Handle translations
        QVector3D translation;
        if (event->key() == (Qt::Key_W))
        {
            translation -= m_camera.forward();
        }
        if (event->key() == (Qt::Key_S))
        {
            translation += m_camera.forward();
        }
        if (event->key() == (Qt::Key_A))
        {
            translation += m_camera.right();
        }
        if (event->key() == (Qt::Key_D))
        {
            translation -= m_camera.right();
        }
        if (event->key() == (Qt::Key_Q))
        {
            translation += m_camera.up();
        }
        if (event->key() == (Qt::Key_E))
        {
            translation -= m_camera.up();
        }
        m_camera.translate(transSpeed * translation);
    }
}

void SceneManager::handleMouse(QMouseEvent *event, float dx, float dy)
{
    static const float rotSpeed = 0.5f;
    // Handle rotations

    if (event->buttons() & Qt::LeftButton) {
        //Do sth else
    } else if (event->buttons() & Qt::RightButton) {
        m_camera.rotate(-rotSpeed * dx, Camera::LocalUp);
        m_camera.rotate(-rotSpeed * dy, m_camera.right());
    }
}

void SceneManager::createShape(const std::string &type)
{
    if(type == "Cube"){
        float x = qrand() % 5;
        float y = qrand() % 5;
        float z = qrand() % 5;
        QVector3D p(x, y, z);

        auto al = AssetLoader::getInstance();
        auto mesh = al->getMesh("Cube");
        int ran = (qrand() % 3) + 1;
        auto mat = al->getMaterial(std::string("Material")+std::to_string(ran));
        Cube *c = new Cube(p, mesh, mat);

        m_shapes.push_back(c);
    } 
    else{
        qCritical() << "Unknown shape";
    }
}

std::string SceneManager::pickShape(int x, int y, int h, int w)
{
    float x_f = (2.0f * x) / w- 1.0f;
    float y_f = 1.0f - (2.0f * y) / h;
    float z_f = 1.0f;
    QVector4D clip (x_f, y_f, -z_f, 1.0f);
    QMatrix4x4 proj_inv = m_proj.inverted(nullptr);
    QVector4D eye = proj_inv * clip;
    eye.setZ(-1);
    eye.setW(0);
    QMatrix4x4 view_inv = m_camera.toMatrix().inverted(nullptr);
    QVector4D wor = view_inv * eye;

    QVector3D ray_wor(wor.x(), wor.y(), wor.z());
    ray_wor.normalize();

    Ray r(-m_camera.translation(), ray_wor);
    size_t i;
    for(i = 0; i < m_shapes.size(); ++i){
        if(m_shapes.at(i)->intersect(r)){
            return std::string("Selected shape id ") + std::to_string(i);
        }
    }
    return std::string("No shape selected");
}
