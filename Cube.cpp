#include "Cube.h"
#include <math.h>

Cube::Cube(const QVector3D &pos, Mesh *mesh, Material *mat)
{
    this->m_world.setToIdentity();
    this->m_world.translate(pos);
    this->m_mesh = mesh;
    this->m_material= mat;
    this->m_name = "Cube";
    this->m_bounds[0] = QVector3D(pos.x() - 0.5, pos.y() - 0.5, pos.z() - 0.5);
    this->m_bounds[1] = QVector3D(pos.x() + 0.5, pos.y() + 0.5, pos.z() + 0.5);

}

bool Cube::intersect(const Ray &r) const
{

    QVector3D dirfrac(1.0f / r.dir.x(), 1.0f / r.dir.y(),1.0f / r.dir.z());

    float t1 = (m_bounds[0].x() - r.orig.x())*dirfrac.x();
    float t2 = (m_bounds[1].x() - r.orig.x())*dirfrac.x();
    float t3 = (m_bounds[0].y() - r.orig.y())*dirfrac.y();
    float t4 = (m_bounds[1].y() - r.orig.y())*dirfrac.y();
    float t5 = (m_bounds[0].z() - r.orig.z())*dirfrac.z();
    float t6 = (m_bounds[1].z() - r.orig.z())*dirfrac.z();

    float tmin = fmax(fmax(fmin(t1, t2), fmin(t3, t4)), fmin(t5, t6));
    float tmax = fmin(fmin(fmax(t1, t2), fmax(t3, t4)), fmax(t5, t6));

    if (tmax < 0)
    {
        return false;
    }

    if (tmin > tmax)
    {
        return false;
    }

    return true;
}

Cube::~Cube()
{
}
