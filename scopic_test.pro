HEADERS       = GLWidget.h \
                Window.h \
                Camera.h \
                Mesh.h \
		Material.h \
		SceneManager.h \
		AssetLoader.h\
		Axis.h\
		Cube.h \ 
		Shape.h
SOURCES       = GLWidget.cpp \
                main.cpp \
                Window.cpp \
                Camera.cpp \
                Mesh.cpp \
		Material.cpp \
		SceneManager.cpp \
		Axis.cpp \
		AssetLoader.cpp \
		Cube.cpp

QT           += widgets

OBJECTS_DIR =./obj/
