#ifndef AXIS_H
#define AXIS_H 

#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLFunctions>
#include <QVector>

class Axis 
{
public:
    Axis();
    ~Axis();
    void init(QOpenGLShaderProgram *program);
    QOpenGLVertexArrayObject &getVAO();

private:
    QOpenGLVertexArrayObject m_vao;
    QOpenGLBuffer m_arrayBuf;
};

#endif // AXIS_H
