#include "GLWidget.h"
#include "Window.h"
#include <QSlider>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QKeyEvent>
#include <QPushButton>
#include <QDesktopWidget>
#include <QApplication>
#include <QMessageBox>

Window::Window()
{
    m_glWidget = new GLWidget;

    QVBoxLayout *mainLayout = new QVBoxLayout;
    QHBoxLayout *container = new QHBoxLayout;
    container->addWidget(m_glWidget);


    QWidget *w = new QWidget;
    w->setLayout(container);
    mainLayout->addWidget(w);
    m_cubeBtn = new QPushButton(tr("Add cube"), this);
    m_label = new QLabel(this);
    m_label->setText("");
    m_label->setMaximumSize(300, 30);
    mainLayout->addWidget(m_label);

    connect(m_cubeBtn, &QPushButton::clicked, m_glWidget, &GLWidget::addCube);
    connect(m_glWidget, &GLWidget::shapeSelected, this, &Window::setLabel);
    mainLayout->addWidget(m_cubeBtn);

    setLayout(mainLayout);
    setWindowTitle(tr("Scopic viewer"));
}

void Window::keyPressEvent(QKeyEvent *e)
{
    if (e->key() == Qt::Key_Escape)
        close();
    else
        this->m_glWidget->keyPressEvent(e);
}


void Window::setLabel(std::string text)
{
    this->m_label->setText(text.c_str());
}
