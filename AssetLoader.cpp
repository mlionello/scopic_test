#include "AssetLoader.h"

AssetLoader* AssetLoader::assetLoader_= nullptr;

AssetLoader::AssetLoader()
{
    loadAssets();
}

AssetLoader::~AssetLoader()
{}


AssetLoader *AssetLoader::getInstance()
{
    if(assetLoader_==nullptr){
        assetLoader_ = new AssetLoader();
    }
    return assetLoader_;
}

void AssetLoader::loadAssets()
{
    {
        float vertices[] = {
            -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
            0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
            0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
            0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
            -0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
            -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,

            -0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
            0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
            0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
            0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
            -0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
            -0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,

            -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,
            -0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
            -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
            -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
            -0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,
            -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,

            0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
            0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
            0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
            0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
            0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
            0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,

            -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,
            0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,
            0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
            0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
            -0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
            -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,

            -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,
            0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,
            0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
            0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
            -0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
            -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f
        };

        std::vector<float> verts(vertices, vertices + (sizeof(vertices)/4));

        this->loadMesh(std::string("Cube"), verts);
    }


    {
        QVector3D ambient (0.2f, 0.2f, 0.2f);
        QVector3D diffuse(0.5f, 0.5f, 0.5f);
        QVector3D specular(1.0f, 1.0f, 1.0f);
        float shininess(32.0f);

        this->loadMaterial(std::string("Material1"), ambient, diffuse, specular, 
                shininess);
    }

    {
        QVector3D ambient (0.1f, 0.1f, 0.7f);
        QVector3D diffuse(0.4f, 0.4f, 1.0f);
        QVector3D specular(0.8f, 0.8f, 0.8f);
        float shininess(8.0f);

        this->loadMaterial(std::string("Material2"), ambient, diffuse, specular, 
                shininess);
    }

    {
        QVector3D ambient (0.4f, 0.1f, 0.1f);
        QVector3D diffuse(1.0f, 0.2f, 0.1f);
        QVector3D specular(1.0f, 1.0f, 1.0f);
        float shininess(64.0f);

        this->loadMaterial(std::string("Material3"), ambient, diffuse, specular, 
                shininess);
    }

}

Mesh *AssetLoader::loadMesh(const std::string &name, 
        const std::vector<float> &vertices)
{
    auto it = this->meshes.find(name);

    if(it != this->meshes.end()){
        return it->second;
    }

    Mesh *m = new Mesh(vertices);

    this->meshes.insert(std::make_pair(name, m));

    return m;
}

Material *AssetLoader::loadMaterial(const std::string &name, QVector3D &amb,
        QVector3D &df, QVector3D &sp, float sh)
{
    auto it = this->materials.find(name);

    if(it != this->materials.end()){
        return it->second;
    }

    Material *m = new Material(amb, df, sp, sh);

    this->materials.insert(std::make_pair(name, m));

    return m;
}

Mesh *AssetLoader::getMesh(const std::string &name)
{
    auto it = this->meshes.find(name);

    if(it != this->meshes.end()){
        return it->second;
    }

    return nullptr;
}

Material *AssetLoader::getMaterial(const std::string &name)
{
    auto it = this->materials.find(name);

    if(it != this->materials.end()){
        return it->second;
    }

    return nullptr;
}
