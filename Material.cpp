#include "Material.h"
#include <QOpenGLShaderProgram>
#include <QCoreApplication>

Material::Material()
    :m_ambient(QVector3D(0.2f, 0.2f, 0.2f)),
    m_diffuse(QVector3D(0.5f, 0.5f, 0.5f)),
    m_specular(QVector3D(1.0f, 1.0f, 1.0f)),
    m_shininess(32.0f)
{}

Material::Material(QVector3D &amb, QVector3D &df, QVector3D &sp, float sh)
    :m_ambient(amb),
    m_diffuse(df),
    m_specular(sp),
    m_shininess(sh)
{}

Material::~Material()
{}

void Material::init(QOpenGLShaderProgram *program)
{
    this->m_ambient_p = program->uniformLocation("material.ambient");
    this->m_diffuse_p = program->uniformLocation("material.diffuse");
    this->m_specular_p = program->uniformLocation("material.specular");
    this->m_shininess_p = program->uniformLocation("material.shininess");
}

void Material::use(QOpenGLShaderProgram *program)
{
    program->setUniformValue(this->m_ambient_p, (GLfloat)this->m_ambient.x(), 
            (GLfloat)this->m_ambient.y(), (GLfloat)this->m_ambient.z());
    program->setUniformValue(this->m_diffuse_p, (GLfloat)this->m_diffuse.x(), 
            (GLfloat)this->m_diffuse.y(), (GLfloat)this->m_diffuse.z());
    program->setUniformValue(this->m_specular_p, (GLfloat)this->m_specular.x(),
            (GLfloat)this->m_specular.y(), (GLfloat)this->m_specular.z());
    program->setUniformValue(this->m_shininess_p, (GLfloat)this->m_shininess);
}
