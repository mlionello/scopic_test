#ifndef RAY_H
#define RAY_H
#include <QVector>

class Ray 
{ 
    public: 
        Ray(const QVector3D &orig, const QVector3D &dir) : orig(orig), dir(dir) 
    {} 

    QVector3D orig, dir;
}; 
#endif
