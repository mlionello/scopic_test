#ifndef SCENEMANAGER_H 

#include "Camera.h"
#include "Mesh.h"
#include "AssetLoader.h"
#include <QMouseEvent>
#include <QOpenGLShaderProgram>
#include <QOpenGLFunctions>
#include <QOpenGLVertexArrayObject>
#include <QCoreApplication>
#include <math.h>
#include <iostream>
#include "Cube.h"
#include "Axis.h"

class SceneManager 
{
public:
    static const char *vertexShaderShapes;
    static const char *fragmentShaderShapes;
    static const char *vertexShaderAxis;
    static const char *fragmentShaderAxis;

    SceneManager();
    ~SceneManager();

    void initGL();
    void resizeScreen(int h, int w);
    void drawAll();

    void handleKeyPressed(QKeyEvent *event);
    void handleMouse(QMouseEvent *event, float dx, float dy);

    void createShape(const std::string &type);
    std::string pickShape(int x, int y, int h, int w);

private:

    QOpenGLShaderProgram *m_program;
    QOpenGLShaderProgram *m_axis_program;

    int m_vp;
    int m_model;
    int m_axis_vp;
    QMatrix4x4 m_proj;
    Camera m_camera;

    Axis axis;
    std::vector<Shape*> m_shapes;

};

#endif // SCENEMANAGER
