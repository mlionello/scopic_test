#ifndef CUBE_H
#define CUBE_H 

#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QOpenGLFunctions>
#include <QVector>
#include "Shape.h"

class Cube : public Shape
{
public:
    ~Cube();
    Cube(const QVector3D &pos, Mesh *mesh, Material *mat);

    virtual bool intersect(const Ray &r) const override;

private:
    QVector3D m_bounds[2];
};

#endif // Cube_H
