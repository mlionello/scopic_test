#ifndef WINDOW_H
#define WINDOW_H

#include <QWidget>
#include <QLabel>
#include <QPushButton>

class GLWidget;

class Window : public QWidget
{
    Q_OBJECT

public:
    Window();

public slots:
    void setLabel(std::string text);

protected:
    void keyPressEvent(QKeyEvent *event) override;

private:
    GLWidget *m_glWidget;
    QPushButton *m_cubeBtn;
    QLabel *m_label;
};

#endif
