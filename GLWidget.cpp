#include "GLWidget.h"
#include "Camera.h"
#include "Mesh.h"
#include <QMouseEvent>
#include <QOpenGLShaderProgram>
#include <QCoreApplication>
#include <math.h>
#include <iostream>
#include "Cube.h"

GLWidget::GLWidget(QWidget *parent)
    : QOpenGLWidget(parent)
{}

GLWidget::~GLWidget()
{}

QSize GLWidget::minimumSizeHint() const
{
    return QSize(50, 50);
}

QSize GLWidget::sizeHint() const
{
    return QSize(400, 400);
}

void GLWidget::initializeGL()
{
    initializeOpenGLFunctions();
    m_sm.initGL();
    m_timer.start(12, this);
}

void GLWidget::paintGL()
{
    m_sm.drawAll();
}

void GLWidget::resizeGL(int w, int h)
{
    m_sm.resizeScreen(h, w);
}

void GLWidget::mousePressEvent(QMouseEvent *event)
{
    m_lastPos = event->pos();
    std::string res = m_sm.pickShape(event->x(), event->y(), this->height(), 
            this->width());

    emit shapeSelected(res);
}

void GLWidget::mouseMoveEvent(QMouseEvent *event)
{
    int dx = event->x() - m_lastPos.x();
    int dy = event->y() - m_lastPos.y();
    m_sm.handleMouse(event, dx, dy);
    m_lastPos = event->pos();
}

void GLWidget::keyPressEvent(QKeyEvent *event)
{
    m_sm.handleKeyPressed(event);
}

void GLWidget::keyReleaseEvent(QKeyEvent *event)
{
  if (event->isAutoRepeat())
  {
    event->ignore();
  }
}

void GLWidget::timerEvent(QTimerEvent *)
{
    this->update();
}

void GLWidget::addCube()
{
    m_sm.createShape("Cube");
}
