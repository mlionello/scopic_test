#include "Mesh.h"

Mesh::Mesh(const std::vector<float> &verts)
    :m_arrayBuf(QOpenGLBuffer::VertexBuffer),
    m_vertices(verts)
{}

void Mesh::init(QOpenGLShaderProgram *program)
{

    m_vao.create();
    QOpenGLVertexArrayObject::Binder vaoBinder(&m_vao);
    m_arrayBuf.create();
    this->m_arrayBuf.bind();
    this->m_arrayBuf.allocate(&this->m_vertices[0], m_vertices.size() * sizeof(float));

    program->enableAttributeArray(0);
    program->setAttributeBuffer(0, GL_FLOAT, 0, 3, sizeof(GL_FLOAT)*6);
    program->enableAttributeArray(1);
    program->setAttributeBuffer(1, GL_FLOAT, sizeof(GL_FLOAT)*3, 3, sizeof(GL_FLOAT)*6);

}

QOpenGLVertexArrayObject &Mesh::getVAO()
{
    return m_vao;
}

Mesh::~Mesh()
{
    m_arrayBuf.destroy();
    m_vao.destroy();
}
