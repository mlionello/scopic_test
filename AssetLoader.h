#ifndef ASSETLOADER_H 

#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QOpenGLFunctions>
#include <QVector>
#include <string>
#include <unordered_map>
#include "Mesh.h"
#include "Material.h"

class AssetLoader 
{
public:
    virtual ~AssetLoader();

    AssetLoader(AssetLoader &other) = delete;
    void operator=(const AssetLoader &) = delete;

    static AssetLoader *getInstance();


    Mesh *loadMesh(const std::string &name, const std::vector<float> &vertices);
    Material *loadMaterial(const std::string &name, QVector3D &amb, 
        QVector3D &df, QVector3D &sp, float sh);

    Mesh *getMesh(const std::string &name);
    Material *getMaterial(const std::string &name);


private:
    AssetLoader();

    static AssetLoader *assetLoader_;

    void loadAssets();
    std::unordered_map<std::string, Mesh*> meshes;
    std::unordered_map<std::string, Material*> materials;

};

#endif
