#ifndef MESH_H
#define MESH_H 

#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLFunctions>
#include <QVector>

class Mesh 
{
public:
    Mesh(const std::vector<float> &verts);
    virtual ~Mesh();
    void init(QOpenGLShaderProgram *program);
    QOpenGLVertexArrayObject &getVAO();

private:
    QOpenGLVertexArrayObject m_vao;
    QOpenGLBuffer m_arrayBuf;
    std::vector<float> m_vertices;
};

#endif // MESH_H
