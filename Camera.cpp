#include "Camera.h"

const QVector3D Camera::LocalForward(0.0f, 0.0f, -1.0f);
const QVector3D Camera::LocalUp(0.0f, 1.0f, 0.0f);
const QVector3D Camera::LocalRight(1.0f, 0.0f, 0.0f);

Camera::Camera() :
    m_dirty(true) 
{}

void Camera::translate(float dx, float dy,float dz) 
{
    translate(QVector3D(dx, dy, dz)); 
}

void Camera::rotate(float angle, const QVector3D &axis) 
{
    rotate(QQuaternion::fromAxisAndAngle(axis, angle)); 
}

void Camera::rotate(float angle, float ax, float ay,float az)
{ 
    rotate(QQuaternion::fromAxisAndAngle(ax, ay, az, angle)); 
}

void Camera::setTranslation(float x, float y, float z) 
{ 
    setTranslation(QVector3D(x, y, z)); 
}

void Camera::setRotation(float angle, const QVector3D &axis) 
{ 
    setRotation(QQuaternion::fromAxisAndAngle(axis, angle)); 
}

void Camera::setRotation(float angle, float ax, float ay, float az) 
{ 
    setRotation(QQuaternion::fromAxisAndAngle(ax, ay, az, angle)); 
}

const QVector3D& Camera::translation() const 
{ 
    return m_translation; 
}

const QQuaternion& Camera::rotation() const 
{ 
    return m_rotation; 
}


void Camera::translate(const QVector3D &dt)
{
    m_dirty = true;
    m_translation += dt;
}

void Camera::setToIdentity()
{
    m_world.setToIdentity();
}

void Camera::rotate(const QQuaternion &dr)
{
    m_dirty = true;
    m_rotation = dr * m_rotation;
}

void Camera::setTranslation(const QVector3D &t)
{
    m_dirty = true;
    m_translation = t;
}

void Camera::setRotation(const QQuaternion &r)
{
    m_dirty = true;
    m_rotation = r;
}

const QMatrix4x4 &Camera::toMatrix()
{
    if (m_dirty){
        m_dirty = false;
        m_world.setToIdentity();
        m_world.rotate(m_rotation.conjugate());
        m_world.translate(m_translation);
    }
    return m_world;
}

QVector3D Camera::forward() const
{
    auto fr = m_rotation.rotatedVector(LocalForward);
    return fr;
}

QVector3D Camera::up() const
{
    return m_rotation.rotatedVector(LocalUp);
}

QVector3D Camera::right() const
{
    return m_rotation.rotatedVector(LocalRight);
}
