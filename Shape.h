#ifndef SHAPE_H
#define SHAPE_H 

#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QOpenGLFunctions>
#include <QVector>
#include "Mesh.h"
#include "Ray.h"
#include "Material.h"

class Shape 
{
public:
    Shape()
        :m_name(std::string("Shape"))
    {}
    virtual ~Shape() {};

    virtual bool intersect(const Ray &r) const  = 0;

    Mesh *getMesh() const { return m_mesh;};
    Material *getMaterial() const { return m_material;};
    QMatrix4x4 getTransform() const { return m_world;};



protected:
    std::string m_name;

    Mesh *m_mesh;
    Material *m_material;

    QMatrix4x4 m_world;
};

#endif // Shape_H
