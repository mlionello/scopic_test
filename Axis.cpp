#include "Axis.h"

Axis::Axis()
    :m_arrayBuf(QOpenGLBuffer::VertexBuffer)
{}

void Axis::init(QOpenGLShaderProgram *program)
{

    float vertices[] = {
                        0, 0, 0,
                        10, 0, 0,
                        0, 0, 0,
                        0, 10, 0,
                        0, 0, 0,
                        0, 0, 10};
    m_vao.create();
    QOpenGLVertexArrayObject::Binder vaoBinder(&m_vao);
    m_arrayBuf.create();
    this->m_arrayBuf.bind();
    this->m_arrayBuf.allocate(vertices, 3 * 6 * sizeof(float));

    program->enableAttributeArray(0);
    program->setAttributeBuffer(0, GL_FLOAT, 0, 3, sizeof(GL_FLOAT)*3);

}

QOpenGLVertexArrayObject &Axis::getVAO()
{
    return m_vao;
}

Axis::~Axis()
{
    m_arrayBuf.destroy();
    m_vao.destroy();
}
