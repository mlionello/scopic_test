#ifndef MATERIAL_H
#define MATERIAL_H 

#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QOpenGLFunctions>
#include <QVector>

class Material 
{
public:
    Material();
    Material(QVector3D &amb, QVector3D &df, QVector3D &sp, float sh);
    virtual ~Material();

    void init(QOpenGLShaderProgram *program);
    void use(QOpenGLShaderProgram *program);

private:

    QVector3D m_ambient;
    QVector3D m_diffuse;
    QVector3D m_specular;
    float m_shininess;

    GLuint m_ambient_p;
    GLuint m_diffuse_p;
    GLuint m_specular_p;
    GLuint m_shininess_p;
};

#endif // MATERIAL_H
