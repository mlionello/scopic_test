# Scopic interview test


## Installation
qmake

Make -j threads


## Usage

./scopic_test

- A(left), S(backwards), D(right), Q(down), W(forward) and E(up) to move the camera.

- Mouse right click to rotate the camera.

- Mouse left click to select cubes.

- Escape exits.

- The light source is at the origin (0, 0, 0)
